package com.viettel.news_backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NewsDto {
    private String id;
    private String categoryId;
    private String category;
    private String imageCategory;
    private String title;
    private String shortLink;
    private String summary;
    private String content;
    @Temporal(TemporalType.DATE)
    private Date createdAt;
    private String createdBy;
    private Integer hotNews;
    private String tags;
    private String imageUrl;
    private String videoUrl;
    @Transient
    private List<NewsDto> similarNews;
}
