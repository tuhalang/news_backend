package com.viettel.news_backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestDto {

    private String isdn;
    private String wsCode;
    private String token;
    private LinkedHashMap<String, Object> wsRequest;
}
