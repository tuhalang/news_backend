package com.viettel.news_backend.controller;

import com.viettel.news_backend.dto.RequestDto;
import com.viettel.news_backend.dto.ResponseDto;
import com.viettel.news_backend.service.NewsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/secure")
@Slf4j
public class NewsController {

    private final NewsService newsService;

//    @RequestMapping(value = "/news", method = RequestMethod.GET)
//    public ResponseDto getLatestNews(@RequestParam Integer page,
//                                     @RequestParam Integer size,
//                                     @RequestParam String language,
//                                     @RequestParam(required = false) String categoryId,
//                                     @RequestParam(required = false) String title){
//        return newsService.fetch(page, size, language, title, categoryId);
//    }

    @RequestMapping(value = "/news", method = RequestMethod.POST)
    public ResponseDto getInfo(@RequestBody RequestDto requestDto){
        return newsService.getInfo(requestDto);
    }
}
