package com.viettel.news_backend.service.impl;

import com.viettel.news_backend.domain.Tags;
import com.viettel.news_backend.dto.CategoryDto;
import com.viettel.news_backend.dto.NewsDto;
import com.viettel.news_backend.dto.RequestDto;
import com.viettel.news_backend.dto.ResponseDto;
import com.viettel.news_backend.repo.TagRepo;
import com.viettel.news_backend.service.NewsService;
import com.viettel.news_backend.utils.Constant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class NewsServiceImpl implements NewsService {

    private static final String WS_GET_LATEST_NEWS = "wsGetLatestNews";
    private static final String WS_GET_NEWS_BY_CATEGORY = "wsGetNewsByCategory";
    private static final String WS_GET_NEWS_BY_TAG = "wsGetNewsByTag";
    private static final String WS_GET_NEWS = "wsGetNewsDetail";
    private static final String WS_GET_CATEGORIES = "wsGetCategories";
    private static final String WS_GET_LIST_TAGS = "wsGetListTags";
    private static final String WS_SEARCH = "wsSearch";

    private final JdbcTemplate jdbcTemplate;
    private final TagRepo tagRepo;

    private List<NewsDto> getSimilarNews(Integer size, String newsId, String categoryId, String language){
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withFunctionName("get_similar_news")
                .returningResultSet("news", BeanPropertyRowMapper.newInstance(NewsDto.class));

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("p_size", size)
                .addValue("p_news", newsId)
                .addValue("p_category", categoryId)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(inParams);
        List<NewsDto> news = (List<NewsDto>) out.get("news");
        return news;
    }

    private ResponseDto getLatestNews(Integer page, Integer size, String language, String title) {

        List<NewsDto> news = new ArrayList<>();
        List<CategoryDto> categories = getCategoryFromDB(page, size, "", language);
        categories.forEach(c -> {
            news.addAll(getNewsByCategoryFromDB(0, 1, c.getId(), language));
        });

//        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
//                .withFunctionName("get_latest_news")
//                .returningResultSet("news", BeanPropertyRowMapper.newInstance(NewsDto.class));
//
//        SqlParameterSource inParams = new MapSqlParameterSource()
//                .addValue("p_page", page)
//                .addValue("p_size", size)
//                .addValue("p_key", title)
//                .addValue("p_language", language);
//
//        Map<String, Object> out = simpleJdbcCall.execute(inParams);
//        List<NewsDto> news = (List<NewsDto>) out.get("news");
        news.forEach(n -> {
            n.setSimilarNews(getSimilarNews(3, n.getId(), n.getCategoryId(), language));
        });

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("news", news)
                .build();
    }

    private ResponseDto getNewsByTag(Integer page, Integer size, String tagName, String language) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withFunctionName("get_news_by_tags")
                .returningResultSet("news", BeanPropertyRowMapper.newInstance(NewsDto.class));

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("p_page", page)
                .addValue("p_size", size)
                .addValue("p_tag", tagName)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(inParams);
        List<NewsDto> news = (List<NewsDto>) out.get("news");

//        news.forEach(n -> {
//            n.setSimilarNews(getSimilarNews(3, n.getId(), n.getCategoryId(), language));
//        });

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("news", news)
                .build();
    }

    private List<NewsDto> getNewsByCategoryFromDB(Integer page, Integer size, String category, String language) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withFunctionName("get_news_by_category")
                .returningResultSet("news", BeanPropertyRowMapper.newInstance(NewsDto.class));

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("p_page", page)
                .addValue("p_size", size)
                .addValue("p_category", category)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(inParams);
        List<NewsDto> news = (List<NewsDto>) out.get("news");

        return news;
    }

    private ResponseDto getNewsByCategory(Integer page, Integer size, String category, String language) {

        List<NewsDto> news = getNewsByCategoryFromDB(page, size, category, language);
        List<Tags> topTags = tagRepo.getTopTagByCategory(category);

//        news.forEach(n -> {
//            n.setSimilarNews(getSimilarNews(3, n.getId(), n.getCategoryId(), language));
//        });

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("news", news)
                .onPushData("topTags", topTags)
                .build();
    }

    private List<CategoryDto> getCategoryFromDB(Integer page, Integer size, String parentId, String language){
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withFunctionName("get_categories")
                .returningResultSet("categories", BeanPropertyRowMapper.newInstance(CategoryDto.class));

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("p_page", page)
                .addValue("p_size", size)
                .addValue("p_parent_id", parentId)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(inParams);
        List<CategoryDto> categories = (List<CategoryDto>) out.get("categories");
        return categories;
    }
    private ResponseDto getCategory(Integer page, Integer size, String parentId, String language) {

        List<CategoryDto> categories = getCategoryFromDB(page, size, parentId, language);
        categories.forEach(e -> {
            e.setSubCategories(getCategoryFromDB(page, size, e.getId(), language));
        });

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("categories", categories)
                .build();
    }

    private ResponseDto getNews(String newsId, String shortLink, String language) {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withFunctionName("get_news")
                .returningResultSet("news", BeanPropertyRowMapper.newInstance(NewsDto.class));

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("p_id", newsId)
                .addValue("p_short_link", shortLink)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(inParams);
        List<NewsDto> news = (List<NewsDto>) out.get("news");

//        news.forEach(n -> {
//            n.setSimilarNews(getSimilarNews(3, n.getId(), n.getCategoryId(), language));
//        });

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("news", news)
                .build();
    }

    @Override
    public ResponseDto getInfo(RequestDto requestDto) {

        String language = "pt";
        Integer page = 0;
        Integer size = 10;
        String title = "";
        String category = "";
        String tag = "";
        String newsId = "";
        String shortLink = "";
        String parentId = "";

        String isdn = requestDto.getIsdn();
        String token = requestDto.getToken();
        String wsCode = requestDto.getWsCode();
        LinkedHashMap<String, Object> wsRequest = requestDto.getWsRequest();

        if(ObjectUtils.isEmpty(isdn) || ObjectUtils.isEmpty(token) || ObjectUtils.isEmpty(wsCode)){
            return ResponseDto.builder()
                    .errorCode(Constant.ERROR_CODE_NOK)
                    .message(Constant.MSG_NOT_PERMISSION)
                    .build();
        }

        if(wsRequest.containsKey("language")){
            language = wsRequest.get("language").toString();
        }

        if(isValid(isdn, token)){

            if(wsRequest.containsKey("page")){
                page = Integer.parseInt(wsRequest.get("page").toString());
            }
            if(wsRequest.containsKey("size")){
                size = Integer.parseInt(wsRequest.get("size").toString());
            }
            if(wsRequest.containsKey("parentId")){
                parentId = wsRequest.get("parentId").toString();
            }
            if(wsRequest.containsKey("newsId")){
                newsId = wsRequest.get("newsId").toString();
            }
            if(wsRequest.containsKey("shortLink")){
                shortLink = wsRequest.get("shortLink").toString();
            }
            if(wsRequest.containsKey("title")){
                title = wsRequest.get("title").toString();
            }
            if(wsRequest.containsKey("category")){
                category = wsRequest.get("category").toString();
            }
            if(wsRequest.containsKey("teg")){
                tag = wsRequest.get("tag").toString();
            }

            if(WS_GET_CATEGORIES.equals(wsCode)){
                return getCategory(page, size, parentId, language);
            }else if(WS_GET_NEWS.equals(wsCode)){
                return getNews(newsId, shortLink, language);
            }else if(WS_GET_LATEST_NEWS.equals(wsCode)){
                return getLatestNews(page, size, language, title);
            }else if(WS_GET_NEWS_BY_CATEGORY.equals(wsCode)){
                return getNewsByCategory(page, size, category, language);
            }else if(WS_GET_NEWS_BY_TAG.equals(wsCode)){
                return getNewsByTag(page, size, tag, language);
            }else if(WS_GET_LIST_TAGS.equals(wsCode)){
                return ResponseDto.builder()
                        .errorCode(Constant.ERROR_CODE_OK)
                        .message(Constant.MSG_SUCCESS)
                        .onPushData("tags",
                                tagRepo.findByTagNameContainingIgnoreCase(PageRequest.of(page, size), "").getContent())
                        .build();
            }else if(WS_SEARCH.equals(wsCode)){
                return search(page, size, category, tag, title, language);
            }
        }

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_NOK)
                .message(Constant.MSG_NOT_PERMISSION)
                .build();
    }

    private boolean isValid(String isdn, String token) {
        return true;
    }

    private ResponseDto search(Integer page, Integer size, String category, String tag, String title, String language) {

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withFunctionName("search_news")
                .returningResultSet("news", BeanPropertyRowMapper.newInstance(NewsDto.class));

        SqlParameterSource inParams = new MapSqlParameterSource()
                .addValue("p_page", page)
                .addValue("p_size", size)
                .addValue("p_category", category)
                .addValue("p_tag", tag)
                .addValue("p_key", title)
                .addValue("p_language", language);

        Map<String, Object> out = simpleJdbcCall.execute(inParams);
        List<NewsDto> news = (List<NewsDto>) out.get("news");

//        news.forEach(n -> {
//            n.setSimilarNews(getSimilarNews(3, n.getId(), n.getCategoryId(), language));
//        });

        return ResponseDto.builder()
                .errorCode(Constant.ERROR_CODE_OK)
                .message(Constant.MSG_SUCCESS)
                .onPushData("news", news)
                .build();
    }
}
