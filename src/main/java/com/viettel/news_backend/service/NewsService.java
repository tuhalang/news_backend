package com.viettel.news_backend.service;

import com.viettel.news_backend.dto.RequestDto;
import com.viettel.news_backend.dto.ResponseDto;

public interface NewsService {
    ResponseDto getInfo(RequestDto requestDto);
}
