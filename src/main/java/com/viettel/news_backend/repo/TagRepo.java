package com.viettel.news_backend.repo;

import com.viettel.news_backend.domain.Tags;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepo extends JpaRepository<Tags, String> {

    Page<Tags> findByTagNameContainingIgnoreCase(Pageable pageable, String tagName);

    List<Tags> findByTagName(String tagName);

    @Query(
            value = "select *" +
                    "from (" +
                    "         select nt.TAG_NAME, count(nt.NEWS_ID) description" +
                    "         from tags t," +
                    "              news_tags nt," +
                    "              news n" +
                    "         where t.TAG_NAME = nt.TAG_NAME" +
                    "           and nt.NEWS_ID = n.ID" +
                    "           and n.CATEGORY_ID = :category" +
                    "         group by (nt.TAG_NAME)" +
                    "         order by count(nt.NEWS_ID) desc)" +
                    "where rownum < 5",
            nativeQuery = true
    )
    List<Tags> getTopTagByCategory(String category);
}
