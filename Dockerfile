FROM adoptopenjdk/openjdk8
MAINTAINER tuhalang
COPY target/NEWS.jar NEWS.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar"]