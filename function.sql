create FUNCTION get_categories(
    p_page in number,
    p_size in number,
    p_parent_id in varchar2,
    p_language in varchar2)
    RETURN SYS_REFCURSOR
AS
    o_cursor SYS_REFCURSOR;
    v_start  number := 0;
    v_end    number := 0;
BEGIN
    v_start := p_page * p_size;
    v_end := (p_page + 1) * p_size;

    if p_parent_id <> null and p_parent_id <> ''
    then
        OPEN o_cursor FOR
            select *
            from (select c.ID                                                                   as ID,
                         decode(p_language, 'en', c.NAME_EN, c.NAME_LC)                         as NAME,
                         decode(p_language, 'en', c.IMAGE_URL_EN, c.IMAGE_URL_LC)               as IMAGE_URL,
                         decode(p_language, 'en', c.DESC_EN, c.DESC_LC)                         as DESCRIPTION,
                         c.PARENT_ID                                                            as PARENT_ID
                  from CATEGORY c
                  where c.PARENT_ID = p_parent_id order by decode(p_language, 'en', c.NAME_EN, c.NAME_LC) ) x
            where ROWNUM > v_start
              and ROWNUM < v_end;
    else
        OPEN o_cursor FOR
            select *
            from (select c.ID                                                                   as ID,
                         decode(p_language, 'en', c.NAME_EN, c.NAME_LC)                         as NAME,
                         decode(p_language, 'en', c.IMAGE_URL_EN, c.IMAGE_URL_LC)               as IMAGE_URL,
                         decode(p_language, 'en', c.DESC_EN, c.DESC_LC)                         as DESCRIPTION,
                         c.PARENT_ID                                                            as PARENT_ID
                  from CATEGORY c
                  where c.PARENT_ID is null order by decode(p_language, 'en', c.NAME_EN, c.NAME_LC) ) x
            where ROWNUM > v_start
              and ROWNUM < v_end;
    end if;

    RETURN o_cursor;
END;
/

create FUNCTION get_latest_news(
    p_page in number,
    p_size in number,
    p_key in varchar2,
    p_language in varchar2)
    RETURN SYS_REFCURSOR
AS
    o_cursor SYS_REFCURSOR;
    v_start  number := 0;
    v_end    number := 0;
BEGIN
    v_start := p_page * p_size;
    v_end := (p_page + 1) * p_size;

    OPEN o_cursor FOR
        select *
        from (select n.ID                                                                   as ID,
                     decode(p_language, 'en', c.NAME_EN, c.NAME_LC)                         as CATEGORY,
                     decode(p_language, 'en', c.IMAGE_URL_EN, c.IMAGE_URL_LC)               as IMAGE_CATEGORY,
                     decode(p_language, 'en', n.TITLE_EN, n.TITLE_LC)                       as TITLE,
                     n.SHORT_LINK                                                           as SHORT_LINK,
                     decode(p_language, 'en', n.SUMMARY_EN, n.SUMMARY_LC)                   as SUMMARY,
                     decode(p_language, 'en', n.CONTENT_EN, n.CONTENT_LC)                   as CONTENT,
                     n.CREATED_AT                                                           as CREATED_AT,
                     n.CREATED_BY                                                           as CREATED_BY,
                     n.HOT_NEWS                                                             as HOT_NEWS,
                     (select WM_CONCAT(TAG_NAME) from NEWS_TAGS nt where nt.NEWS_ID = n.ID) as TAGS
              from news n,
                   category c
              where n.CATEGORY_ID = c.ID
                and lower(decode(p_language, 'en', n.TITLE_EN, n.TITLE_LC)) like '%' || lower(p_key) || '%'
              order by n.HOT_NEWS desc, CREATED_AT desc) x
        where ROWNUM > v_start
          and ROWNUM < v_end;

    RETURN o_cursor;
END;
/

create FUNCTION get_news(
    p_id in number,
    p_short_link in number,
    p_language in varchar2)
    RETURN SYS_REFCURSOR
AS
    o_cursor SYS_REFCURSOR;
BEGIN

    if p_id <> null and p_id <> ''
    then
        OPEN o_cursor FOR
            select n.ID                                                                   as ID,
                   decode(p_language, 'en', c.NAME_EN, c.NAME_LC)                         as CATEGORY,
                   decode(p_language, 'en', c.IMAGE_URL_EN, c.IMAGE_URL_LC)               as IMAGE_CATEGORY,
                   decode(p_language, 'en', n.TITLE_EN, n.TITLE_LC)                       as TITLE,
                   n.SHORT_LINK                                                           as SHORT_LINK,
                   decode(p_language, 'en', n.SUMMARY_EN, n.SUMMARY_LC)                   as SUMMARY,
                   decode(p_language, 'en', n.CONTENT_EN, n.CONTENT_LC)                   as CONTENT,
                   n.CREATED_AT                                                           as CREATED_AT,
                   n.CREATED_BY                                                           as CREATED_BY,
                   n.HOT_NEWS                                                             as HOT_NEWS,
                   (select WM_CONCAT(TAG_NAME) from NEWS_TAGS nt where nt.NEWS_ID = n.ID) as TAGS
            from news n,
                 category c
            where n.CATEGORY_ID = c.ID
              and n.ID = p_id
            order by n.HOT_NEWS desc, CREATED_AT desc;
    else
        OPEN o_cursor FOR
            select n.ID                                                                   as ID,
                   decode(p_language, 'en', c.NAME_EN, c.NAME_LC)                         as CATEGORY,
                   decode(p_language, 'en', c.IMAGE_URL_EN, c.IMAGE_URL_LC)               as IMAGE_CATEGORY,
                   decode(p_language, 'en', n.TITLE_EN, n.TITLE_LC)                       as TITLE,
                   n.SHORT_LINK                                                           as SHORT_LINK,
                   decode(p_language, 'en', n.SUMMARY_EN, n.SUMMARY_LC)                   as SUMMARY,
                   decode(p_language, 'en', n.CONTENT_EN, n.CONTENT_LC)                   as CONTENT,
                   n.CREATED_AT                                                           as CREATED_AT,
                   n.CREATED_BY                                                           as CREATED_BY,
                   n.HOT_NEWS                                                             as HOT_NEWS,
                   (select WM_CONCAT(TAG_NAME) from NEWS_TAGS nt where nt.NEWS_ID = n.ID) as TAGS
            from news n,
                 category c
            where n.CATEGORY_ID = c.ID
              and n.SHORT_LINK = p_short_link
            order by n.HOT_NEWS desc, CREATED_AT desc;
    end if;

    RETURN o_cursor;
END;
/

create FUNCTION get_news_by_category(
    p_page in number,
    p_size in number,
    p_category in varchar2,
    p_language in varchar2)
    RETURN SYS_REFCURSOR
AS
    o_cursor SYS_REFCURSOR;
    v_start  number := 0;
    v_end    number := 0;
BEGIN
    v_start := p_page * p_size;
    v_end := (p_page + 1) * p_size;

    OPEN o_cursor FOR
        select *
        from (select n.ID                                                                   as ID,
                     decode(p_language, 'en', c.NAME_EN, c.NAME_LC)                         as CATEGORY,
                     decode(p_language, 'en', c.IMAGE_URL_EN, c.IMAGE_URL_LC)               as IMAGE_CATEGORY,
                     decode(p_language, 'en', n.TITLE_EN, n.TITLE_LC)                       as TITLE,
                     n.SHORT_LINK                                                           as SHORT_LINK,
                     decode(p_language, 'en', n.SUMMARY_EN, n.SUMMARY_LC)                   as SUMMARY,
                     decode(p_language, 'en', n.CONTENT_EN, n.CONTENT_LC)                   as CONTENT,
                     n.CREATED_AT                                                           as CREATED_AT,
                     n.CREATED_BY                                                           as CREATED_BY,
                     n.HOT_NEWS                                                             as HOT_NEWS,
                     (select WM_CONCAT(TAG_NAME) from NEWS_TAGS nt where nt.NEWS_ID = n.ID) as TAGS
              from news n,
                   category c
              where n.CATEGORY_ID = c.ID
                and c.ID = p_category
              order by n.HOT_NEWS desc, CREATED_AT desc) x
        where ROWNUM > v_start
          and ROWNUM < v_end;

    RETURN o_cursor;
END;
/

create FUNCTION get_news_by_tags(
    p_page in number,
    p_size in number,
    p_tag in varchar2,
    p_language in varchar2)
    RETURN SYS_REFCURSOR
AS
    o_cursor SYS_REFCURSOR;
    v_start  number := 0;
    v_end    number := 0;
BEGIN
    v_start := p_page * p_size;
    v_end := (p_page + 1) * p_size;

    OPEN o_cursor FOR
        select *
        from (select n.ID                                                                   as ID,
                     decode(p_language, 'en', c.NAME_EN, c.NAME_LC)                         as CATEGORY,
                     decode(p_language, 'en', c.IMAGE_URL_EN, c.IMAGE_URL_LC)               as IMAGE_CATEGORY,
                     decode(p_language, 'en', n.TITLE_EN, n.TITLE_LC)                       as TITLE,
                     n.SHORT_LINK                                                           as SHORT_LINK,
                     decode(p_language, 'en', n.SUMMARY_EN, n.SUMMARY_LC)                   as SUMMARY,
                     decode(p_language, 'en', n.CONTENT_EN, n.CONTENT_LC)                   as CONTENT,
                     n.CREATED_AT                                                           as CREATED_AT,
                     n.CREATED_BY                                                           as CREATED_BY,
                     n.HOT_NEWS                                                             as HOT_NEWS,
                     (select WM_CONCAT(TAG_NAME) from NEWS_TAGS nt where nt.NEWS_ID = n.ID) as TAGS
              from news n,
                   category c,
                   NEWS_TAGS nt
              where n.CATEGORY_ID = c.ID
                and nt.NEWS_ID = n.ID
                and nt.TAG_NAME = p_tag
              order by n.HOT_NEWS desc, CREATED_AT desc) x
        where ROWNUM > v_start
          and ROWNUM < v_end;

    RETURN o_cursor;
END;
/


